import sys,os
import numpy as np


def counter(M,denominations):
# Need to supply the total amount and a numpy array of denominations. THis works only for four denominations

#Sort the denomination list and remove any possible duplicates
    list = np.unique(np.sort(np.array(denominations)))

#Find the maximum coins that can make the amount M for each denomination
    MaxCounter = np.zeros(list.size)
    for i in range(list.size):
        MaxCounter[i] = np.floor(M/list[i])


#Go through all the possible compinations of the coins to find the ones that recreate M.

    Ncounts=0 # The number of combinations

    for i in np.arange(MaxCounter[0]+1):
        for j in np.arange(MaxCounter[1]+1):
            for k in np.arange(MaxCounter[2]+1):
                for l in np.arange(MaxCounter[3]+1):
                    if l*list[3]+k*list[2]+j*list[1]+i*list[0] == M:
                        Ncounts +=1
    return Ncounts


def oddsum(denominations,A,B):
#Sum up the total amount of odd combinations that make the numbers from A to B. A and B must be integers.

#Total Sum counter
    Nsum = 0

    for i in np.arange(A,B+1):
        TempCounter = counter(i,denominations)
       
        if TempCounter%2 != 0: #Test if odd
            Nsum += TempCounter
    return Nsum
        


#Main Program


denominations = (1,5,10,25)

print(oddsum(denominations,1,100))
